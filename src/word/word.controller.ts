import { Body, Controller, Get, Post, Res, UseGuards } from '@nestjs/common';
import { WordService } from './word.service';
import { WordCheckRequest } from './word.check.request';
import { Response } from 'express';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse
} from "@nestjs/swagger";

@ApiBearerAuth()
@ApiTags("word")
@UseGuards(JwtAuthGuard)
@Controller('word')
export class WordController {
  constructor(private serv: WordService) {}

  @Post('check')
  @ApiOperation({ summary: 'Check word' })
  @ApiOkResponse({ description: 'check word.' })
  @ApiUnauthorizedResponse({
    description: 'User not found or user unauthorized.',
  })
  public async checkWord(
    @Body() wordRequestCheck: WordCheckRequest,
    @Res() res: Response,
  ) {
    return this.serv.checkWordActual(wordRequestCheck, res);
  }
}
