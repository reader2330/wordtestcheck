import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Word } from './word.entity';
import { WordCheckRequest } from './word.check.request';
import { WordUtil } from './word.util';
import { WinsService } from '../wins/wins.service';
import { Response } from 'express';
import { ResponseLetter, WordCheckResponse } from './word.check.response';
import {
  MAX_ATTEMPS,
  MAX_ATTEMPS_MSG,
  VALUE_EQUAL_POSITION,
  VALUE_NOT_CONTAINS,
  VALUE_NOT_EQUAL_POSITION,
} from '../constant';

import { GameService } from '../game/game.service';

import { GameUserService } from '../gameUser/game.user.service';

@Injectable()
export class WordService {
  constructor(
    @InjectRepository(Word)
    private wordRepository: Repository<Word>,
    private winsService: WinsService,
    @Inject(forwardRef(() => GameService))
    private gameService: GameService,
    private gameUserService: GameUserService,
  ) {}

  async checkWordActual(wordCheckRequest: WordCheckRequest, res: Response) {
    const response = new WordCheckResponse();
    const userWord = wordCheckRequest.user_word.toUpperCase();
    const gameActual = await this.gameService.getGameActual();
    if (!gameActual || !gameActual.word) {
      response.msg = 'no_current_game';
      return res.status(200).json(response);
    }
    const gameUserActual = await this.gameUserService.getGameUser(
      wordCheckRequest.userId,
      gameActual.game_id,
    );
    if (gameUserActual.attemps >= MAX_ATTEMPS) {
      response.win = false;
      response.msg = MAX_ATTEMPS_MSG;
      return res.status(200).json(response);
    }
    if (WordUtil.checkEqualsWord(userWord, gameActual.word)) {
      response.win = true;
      await this.winsService.setWin(userWord, wordCheckRequest.userId);
      //await this.gameService.disableGame(gameActual);
      return res.status(200).json(response);
    }
    response.win = false;
    response.response = this.checkLetters(userWord, gameActual.word);
    await this.gameUserService.addAttemp(gameUserActual);
    return res.status(200).json(response);
  }

  checkLetters(userWord: string, actualWord: string): ResponseLetter[] {
    const responses: ResponseLetter[] = [];
    [...userWord].forEach((letter, index) => {
      if (!WordUtil.checkContainLetter(letter, actualWord)) {
        responses.push(new ResponseLetter(letter, VALUE_NOT_CONTAINS));
        return;
      }
      if (!WordUtil.checkEqualPosition(letter, index, actualWord)) {
        responses.push(new ResponseLetter(letter, VALUE_NOT_EQUAL_POSITION));
        return;
      }
      responses.push(new ResponseLetter(letter, VALUE_EQUAL_POSITION));
    });
    return responses;
  }

  public async getWord(): Promise<Word> {
    return await this.wordRepository.findOneBy({
      isUse: false,
      enabled: true,
    });
  }
  public async disableWord(word: Word) {
    word.isUse = true;
    word.udate = new Date();
    await this.wordRepository.update(
      { word_id: word.word_id, enabled: true },
      word,
    );
  }

  findAll(): Promise<Word[]> {
    return this.wordRepository.find();
  }
}
