export class WordCheckResponse {
  win: boolean;

  response: ResponseLetter[];

  msg: string;
}

export class ResponseLetter {
  letter: string;
  value: number;

  constructor(letter: string, value: number) {
    this.value = value;
    this.letter = letter;
  }
}
