import { IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from "@nestjs/swagger";

export class WordCheckRequest {
  @ApiProperty()
  @IsNotEmpty()
  @Length(5, 5)
  user_word: string;
  @IsNotEmpty()
  @ApiProperty()
  userId: number;
}
