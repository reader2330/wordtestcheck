import { Test, TestingModule } from '@nestjs/testing';
import { WordService } from './word.service';
import { WordModule } from "./word.module";
import { getRepositoryToken } from "@nestjs/typeorm";
import { User } from "../user/user.entity";
import { Wins } from "../wins/wins.entity";
import { GameUser } from "../gameUser/gameuser.entity";
import { Word } from "./word.entity";
import { Game } from "../game/game.entity";

describe('WordService', () => {
  let service: WordService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [WordModule],
    }).overrideProvider(getRepositoryToken(User))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Wins))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(GameUser))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Word))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Game))
      .useValue(jest.fn())
      .compile();

    service = module.get<WordService>(WordService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
