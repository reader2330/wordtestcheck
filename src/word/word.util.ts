export class WordUtil {
  public static checkEqualsWord(userWord: string, actualWord: string): boolean {
    return userWord == actualWord;
  }
  public static checkContainLetter(
    letter: string,
    actualWord: string,
  ): boolean {
    return actualWord.includes(letter);
  }

  public static checkEqualPosition(
    letterUser: string,
    indexUser: number,
    actualWord: string,
  ): boolean {
    let equal = false;
    [...actualWord].forEach((letter: string, index: number) => {
      if (letter == letterUser && index == indexUser) {
        equal = true;
        return;
      }
    });
    return equal;

  }
}
