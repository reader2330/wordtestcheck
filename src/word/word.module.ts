import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Word } from './word.entity';

import { WordService } from './word.service';
import { WordController } from './word.controller';
import { WinsModule } from '../wins/wins.module';
import { UserModule } from '../user/user.module';
import { GameUserModule } from '../gameUser/game.user.module';
import { GameModule } from '../game/game.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Word]),
    forwardRef(() => GameModule),
    WinsModule,
    UserModule,
    GameUserModule,
  ],
  providers: [WordService],
  controllers: [WordController],
  exports: [WordService],
})
export class WordModule {}
