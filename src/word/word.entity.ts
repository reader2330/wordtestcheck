import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity({
  name: 'os_wordle_word',
})
export class Word {
  @PrimaryGeneratedColumn()
  word_id: number;

  @Column({
    type: 'varchar',
    length: 5,
    unique: true,
    nullable: false,
  })
  word: string;

  @Column({
    type: 'boolean',
    default: true,
    nullable: false,
  })
  enabled: boolean;

  @Column({
    type: 'boolean',
    name: 'is_use',
    default: false,
    nullable: false,
  })
  isUse: boolean;
  @Column({
    type: 'timestamp',
    nullable: true,
  })
  cdate: Date;
  @Column({
    type: 'timestamp',
    nullable: true,
  })
  udate: Date;
}
