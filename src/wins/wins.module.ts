import { Module } from '@nestjs/common';
import { WinsController } from './wins.controller';
import { WinsService } from './wins.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Wins } from './wins.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Wins])],
  controllers: [WinsController],
  providers: [WinsService],
  exports: [WinsService],
})
export class WinsModule {}
