import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity({
  name: 'os_wordle_wins',
})
export class Wins {
  @PrimaryGeneratedColumn()
  wins_id: number;

  @Column({
    type: 'varchar',
    length: 5,
    nullable: false,
  })
  word: string;

  @Column({
    type: 'boolean',
    default: true,
    nullable: false,
  })
  enabled: boolean;

  @Column({
    type: 'int',
    nullable: false,
  })
  user_id: number;
  @Column({
    type: 'timestamp',
    nullable: false,
    default: new Date(),
  })
  cdate: Date;
  @Column({
    type: 'timestamp',
    nullable: true,
  })
  udate: Date;
}
