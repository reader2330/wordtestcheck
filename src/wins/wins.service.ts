import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Wins } from './wins.entity';

@Injectable()
export class WinsService {
  constructor(
    @InjectRepository(Wins)
    private winsRepository: Repository<Wins>,
  ) {}

  async setWin(word: string, userId: number) {
    const win = new Wins();
    win.word = word;
    win.user_id = userId;
    await this.winsRepository.save(win);
  }
  public async getWinsUser(user_id: number): Promise<{
    rows: Wins[];
    count: number;
  }> {
    const result = await this.winsRepository.findAndCount({
      where: {
        user_id: user_id,
        enabled: true,
      },
    });
    return {
      rows: result[0],
      count: result[1],
    };
  }
  public async getRankingUser(): Promise<Wins[]> {
    return await this.winsRepository.query(
      'select user_id, count(*) from os_wordle_wins group BY user_id order by count desc limit 10',
    );
  }
  public async getWordWins(){
    return await this.winsRepository.query(
      'select word, count(*) from os_wordle_wins group BY word order by count desc limit 10',
    );
  }
}
