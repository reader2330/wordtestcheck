import { Controller, Get, UseGuards } from '@nestjs/common';
import { WinsService } from './wins.service';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
@ApiBearerAuth()
@ApiTags('wins')
@UseGuards(JwtAuthGuard)
@Controller('wins')
export class WinsController {
  constructor(private winsService: WinsService) {}
  @Get('rank')
  @ApiOperation({ summary: 'Get rank word' })
  @ApiOkResponse({ description: 'Get rank word.' })
  @ApiUnauthorizedResponse({
    description: 'User not found or user unauthorized.',
  })
  public async getRankWord() {
    return this.winsService.getWordWins();
  }
}
