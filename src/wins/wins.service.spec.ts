import { Test, TestingModule } from '@nestjs/testing';
import { WinsService } from './wins.service';
import { getRepositoryToken } from "@nestjs/typeorm";
import { Wins } from "./wins.entity";
import { WinsModule } from "./wins.module";

describe('WinsService', () => {
  let service: WinsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [WinsModule],
    }).overrideProvider(getRepositoryToken(Wins))
      .useValue(jest.fn())
      .compile();

    service = module.get<WinsService>(WinsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
