export const VALUE_EQUAL_POSITION =
  parseInt(process.env.VALUE_EQUAL_POSITION) || 1;
export const VALUE_NOT_EQUAL_POSITION =
  parseInt(process.env.VALUE_NOT_EQUAL_POSITION) || 2;
export const VALUE_NOT_CONTAINS = parseInt(process.env.VALUE_NOT_CONTAINS) || 3;
export const MAX_ATTEMPS = parseInt(process.env.MAX_ATTEMPS) || 5;
export const TIME_DURATION_GAME = parseInt(process.env.TIME_DURATION_GAME) || 5;
export const MAX_ATTEMPS_MSG = process.env.MAX_ATTEMPS_MSG || 'Attempts passed';
