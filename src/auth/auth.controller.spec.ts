
import { Test } from '@nestjs/testing';
import { AuthController } from './auth.controller';

import { AuthService } from './auth.service';

describe('AuthController', () => {
  let contoller: AuthController;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [AuthService],
    })
      .overrideProvider(AuthService)
      .useValue(jest.fn())
      .compile();
    contoller = moduleRef.get<AuthController>(AuthController);

  });
  it('should define', () => {
      expect(contoller).toBeDefined()
  });

});
