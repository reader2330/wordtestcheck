import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';

import { GameUserService } from '../gameUser/game.user.service';
import { WinsService } from '../wins/wins.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../user/user.entity';
import { AuthModule } from './auth.module';

import { Wins } from '../wins/wins.entity';
import { GameUser } from '../gameUser/gameuser.entity';

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
    })
      .overrideProvider(getRepositoryToken(GameUser))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Wins))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(User))
      .useValue(jest.fn())
      .overrideProvider(GameUserService)
      .useValue(jest.fn())
      .overrideProvider(WinsService)
      .useValue(jest.fn())
      .compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('correct username', async () => {
    const username = 'correct';
    jest
      .spyOn(service, 'validateUser')
      .mockImplementation((username: string) => {
        return Promise.resolve(new User());
      });
    const userActual = await service.validateUser(username);
    expect(userActual).toStrictEqual(new User());
  });
});
