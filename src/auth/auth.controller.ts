import { Body, Controller, Post, Res } from '@nestjs/common';
import { LoginRequest } from './login.request';
import { AuthService } from './auth.service';
import { Response } from 'express';
import {
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('login')
  @ApiOperation({ summary: 'Login user' })
  @ApiOkResponse({ description: 'User valid.' })
  @ApiUnauthorizedResponse({
    description: 'User not found or user unauthorized.',
  })
  public async login(@Body() body: LoginRequest, @Res() res: Response) {
    return await this.authService.login(body, res);
  }
}
