import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { LoginRequest } from './login.request';
import { User } from '../user/user.entity';
import { Response } from "express";

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string): Promise<User> {
    return await this.usersService.findOneUser(username);
  }

  async login(body: LoginRequest, res: Response): Promise<any> {
    const user = await this.validateUser(body.username);
    if (!user) {
      return res.status(401).json({
        msg: 'username_not_valid',
      });
    }

    const payload = { username: user.username, sub: user.user_id };
    return res.status(200).json({
      access_token: this.jwtService.sign(payload),
    });
  }
}
