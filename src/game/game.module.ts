import { CacheModule, forwardRef, Module } from '@nestjs/common';
import { GameService } from './game.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Game } from './game.entity';
import { WordModule } from '../word/word.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Game]),
    CacheModule.register(),
    forwardRef(() => WordModule),
  ],
  providers: [GameService],
  exports: [GameService],
})
export class GameModule {}
