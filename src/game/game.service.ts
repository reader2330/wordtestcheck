import {
  CACHE_MANAGER,
  forwardRef,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cache } from 'cache-manager';

import { Repository } from 'typeorm';
import { Game } from './game.entity';
import { Interval } from '@nestjs/schedule';
import * as moment from 'moment';
import { WordService } from '../word/word.service';
import { TIME_DURATION_GAME } from '../constant';

@Injectable()
export class GameService {
  private readonly logger = new Logger(GameService.name);
  constructor(
    @InjectRepository(Game)
    private gameRepository: Repository<Game>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    @Inject(forwardRef(() => WordService))
    private wordService: WordService,
  ) {}
  @Interval(300000)
  public async createGame(): Promise<Game> {
    const nowUnix = moment(new Date()).unix();
    const gameBefore = await this.gameRepository.findOneBy({ active: true });
    if (gameBefore) {
      if (!this.checkDiff(gameBefore, nowUnix)) {
        return gameBefore;
      }
      gameBefore.udate = new Date();
      gameBefore.active = false;
      await this.gameRepository.update(
        { game_id: gameBefore.game_id },
        gameBefore,
      );
    }
    const now = moment(new Date()).add(TIME_DURATION_GAME, 'm');
    let game = new Game();
    const word = await this.wordService.getWord();
    if (!word) {
      return new Game();
    }
    game.word = word.word;
    game.enabled = true;
    game.active = true;
    game.cdate = new Date();
    game.expired_date = now.unix();
    game = await this.gameRepository.save(game);
    await this.wordService.disableWord(word);
    await this.cacheManager.set('game', game, 30000);
    this.logger.log('create_new_game');
    return game;
  }

  public async getGameActual(): Promise<Game> {
    const now = moment(new Date()).unix();
    let game = await this.cacheManager.get<Game>('game');
    if (!game) {
      game = await this.gameRepository.findOneBy({ active: true });
      if (game) {
        if (this.checkDiff(game, now)) {
          await this.disableGame(game);
          game = await this.createGame();
        }
      } else {
        game = await this.createGame();
      }

      await this.cacheManager.set('game', game, 30000);
      return game;
    }
    if (this.checkDiff(game, now)) {
      await this.disableGame(game);
      game = await this.createGame();
      return game;
    }
    return game;
  }
  public checkDiff(game: Game, ts: number) {
    return game.expired_date < ts;
  }
  public async disableGame(game: Game) {
    game.active = false;
    game.udate = new Date();
    const res = await this.gameRepository.update(
      { game_id: game.game_id },
      game,
    );

    this.logger.log('disabled_game ---->  ' + res.affected);
  }
}
