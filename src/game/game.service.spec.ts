import { Test, TestingModule } from '@nestjs/testing';
import { GameService } from './game.service';
import { WordService } from '../word/word.service';
import { CACHE_MANAGER } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Game } from './game.entity';
import { GameModule } from './game.module';
import { Word } from '../word/word.entity';
import { GameUser } from '../gameUser/gameuser.entity';
import { Wins } from '../wins/wins.entity';
import { User } from '../user/user.entity';
import { Repository } from 'typeorm';

describe('GameService', () => {
  let service: GameService;
  const mockedRepo = {
    // mock the repo `findOneOrFail`
    update: jest.fn((criteria, update) => Promise.resolve({})),
  };
  const mockCache = {
    get: jest.fn((name) => Promise.resolve(new Game()))
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [GameModule],
    })
      .overrideProvider(getRepositoryToken(User))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Wins))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(GameUser))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Word))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Game))
      .useValue(mockedRepo)
      .overrideProvider(WordService)
      .useValue(jest.fn())
      .overrideProvider(CACHE_MANAGER)
      .useValue(mockCache)
      .compile();

    service = module.get<GameService>(GameService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('check_diff', () => {
    const game = new Game();
    game.expired_date = 20;
    expect(service.checkDiff(game, 25)).toStrictEqual(true);
  });

  it('disable_game', async () => {
    const game = new Game();
    game.expired_date = 20;
    game.game_id = 50;
    const res = await service.disableGame(game);
    expect(res).toStrictEqual(undefined);
  });
  it('game_actual_cache', async () => {
    const res = await service.getGameActual();
    expect(res).toStrictEqual(new Game());
  });
});
