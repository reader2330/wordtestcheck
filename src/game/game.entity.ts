import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import * as moment from 'moment'
@Entity({
  name: 'os_wordle_game',
})
export class Game {
  @PrimaryGeneratedColumn()
  game_id: number;

  @Column({
    type: 'varchar',
    length: 5,
    unique: true,
    nullable: false,
  })
  word: string;

  @Column({
    type: 'boolean',
    default: true,
    nullable: false,
  })
  enabled: boolean;
  @Column({
    type: 'boolean',
    default: false,
    nullable: false,
  })
  active: boolean;
  @Column({
    type: 'timestamp',
    nullable: false,
  })
  cdate: Date;
  @Column({
    type: 'int',
    nullable: false,
  })
  expired_date: number;
  @Column({
    type: 'timestamp',

    nullable: true,
  })
  udate: Date;
}
