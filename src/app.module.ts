import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WordModule } from './word/word.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Word } from './word/word.entity';

import { WinsModule } from './wins/wins.module';
import { GameModule } from './game/game.module';
import { UserModule } from './user/user.module';
import { ScheduleModule } from '@nestjs/schedule';
import { GameUserModule } from './gameUser/game.user.module';
import { Wins } from './wins/wins.entity';
import { GameUser } from './gameUser/gameuser.entity';
import { User } from './user/user.entity';
import { Game } from './game/game.entity';
import { ConfigModule } from '@nestjs/config';

import { AuthModule } from './auth/auth.module';

const config = TypeOrmModule.forRootAsync({
  useFactory: () => ({
    type: 'postgres',
    host: process.env.DB_HOST,
    port: parseInt(process.env.PORT) || 5432,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: '',
    synchronize: true,
    entities: [Word, GameUser, Game, User, Wins],
  }),
});
@Module({
  imports: [
    ConfigModule.forRoot(),
    config,
    WordModule,
    WinsModule,
    GameModule,
    UserModule,
    ScheduleModule.forRoot(),
    GameUserModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
