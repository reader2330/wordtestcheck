import { Controller, Get, Param, Res, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { UserService } from './user.service';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { StatsUserResponse } from './stats.user.response';
@ApiBearerAuth()
@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}
  @UseGuards(JwtAuthGuard)
  @Get('stats/:id')
  @ApiOperation({ summary: 'Get user stats by id' })
  @ApiOkResponse({ description: 'Get stats.' })
  @ApiUnauthorizedResponse({
    description: 'User not found or user unauthorized.',
  })
  @ApiParam({
    name: 'id',
    type: 'number',
    description: 'id of user',
    example: 1,
  })
  public async getStatsByUser(@Param('id') id: number, @Res() res: Response) {
    return await this.userService.getStatsByUser(id, res);
  }
  @UseGuards(JwtAuthGuard)
  @Get('ranking')
  @ApiOperation({ summary: 'Get users rank' })
  @ApiOkResponse({ description: 'Get rank.' })
  @ApiUnauthorizedResponse({
    description: 'User not found or user unauthorized.',
  })
  public async getRanking(@Res() res: Response) {
    return await this.userService.getRanking(res);
  }
}
