export class StatsUserResponse {
  user_id: number;
  games: number;
  wins: number;
}
