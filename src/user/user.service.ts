import { Inject, Injectable } from '@nestjs/common';
import { Response } from 'express';
import { StatsUserResponse } from './stats.user.response';
import { GameUserService } from '../gameUser/game.user.service';
import { WinsService } from '../wins/wins.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { RankingUserResponse } from './ranking.user.response';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private gameUserService: GameUserService,
    private winsService: WinsService,
  ) {}
  public async getStatsByUser(user_id: number, res: Response) {
    const response = new StatsUserResponse();
    response.user_id = user_id;
    const resU = await this.gameUserService.getGamesUser(user_id);
    response.games = resU.count;
    const resW = await this.winsService.getWinsUser(user_id);
    response.wins = resW.count;

    return res.status(200).json(response);
  }
  public async getRanking(res: Response) {
    //const response = new RankingUserResponse();
    const resW = await this.winsService.getRankingUser();
    return res.status(200).json(resW);
  }
  public async findOneUser(username: string): Promise<User> {
    return this.userRepository.findOneBy({ username: username });
  }
}
