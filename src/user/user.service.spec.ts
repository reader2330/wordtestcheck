import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

import { GameUser } from '../gameUser/gameuser.entity';
import { Wins } from '../wins/wins.entity';
import { UserModule } from './user.module';
export type MockType<T> = {
  [P in keyof T]?: jest.Mock<{}>;
};
export const repositoryMockFactory: () => MockType<Repository<any>> = jest.fn(
  () => ({
    findOne: jest.fn((entity) => entity),
  }),
);
describe('UserService', () => {
  let service: UserService;
  let repositoryMock: MockType<Repository<User>>;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [UserModule],
    })
      .overrideProvider(getRepositoryToken(Wins))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(GameUser))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(User))
      .useValue(jest.fn())
      .compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
