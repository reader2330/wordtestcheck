import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({
  name: 'os_wordle_user',
})
export class User {
  @PrimaryGeneratedColumn()
  user_id: number;

  @Column({
    type: 'varchar',
    default: true,
    nullable: false,
  })
  username: string;

  @Column({
    type: 'boolean',
    default: true,
    nullable: false,
  })
  enabled: boolean;
  @Column({
    type: 'timestamp',
    nullable: true,

  })
  cdate: Date;
  @Column({
    type: 'timestamp',
    nullable: true,
  })
  udate: Date;
}
