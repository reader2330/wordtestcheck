export class RankingUserResponse {
  users: [{ user_id: number; wins: number }];
}
