import { Test, TestingModule } from '@nestjs/testing';

import { UserController } from './user.controller';

import { UserService } from './user.service';


describe('UserController', () => {
  let contoller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
    })
      .overrideProvider(UserService)
      .useValue(jest.fn())
      .compile();
    contoller = module.get<UserController>(UserController);
  });

  it('should define', () => {
    expect(contoller).toBeDefined();
  });
});
