import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';

import { GameUserModule } from '../gameUser/game.user.module';
import { WinsModule } from '../wins/wins.module';

@Module({
  imports: [TypeOrmModule.forFeature([User]), WinsModule, GameUserModule],
  providers: [UserService],
  exports: [UserService],
  controllers: [UserController],
})
export class UserModule {}
