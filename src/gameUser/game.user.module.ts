import {  Module } from '@nestjs/common';
import { GameUserService } from './game.user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GameUser } from './gameuser.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([GameUser]),
  ],
  providers: [GameUserService],
  exports: [GameUserService],
})
export class GameUserModule {}
