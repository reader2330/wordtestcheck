import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { GameUser } from './gameuser.entity';

@Injectable()
export class GameUserService {
  private readonly logger = new Logger(GameUserService.name);
  constructor(
    @InjectRepository(GameUser)
    private gameUserRepository: Repository<GameUser>,
  ) {}
  public async getGameUser(
    user_id: number,
    game_id: number,
  ): Promise<GameUser> {
    let gameUser = await this.gameUserRepository.findOneBy({
      user_id: user_id,
      game_id: game_id,
      enabled: true,
    });
    if (!gameUser) {
      gameUser = new GameUser();
      gameUser.user_id = user_id;
      gameUser.game_id = game_id;

      return await this.gameUserRepository.save(gameUser);
    }
    return gameUser;
  }

  public async getGamesUser(user_id: number): Promise<{
    rows: GameUser[];
    count: number;
  }> {
    const result = await this.gameUserRepository.findAndCount({
      where: {
        user_id: user_id,
        enabled: true,
      },
    });
    return {
      rows: result[0],
      count: result[1],
    };
  }
  public async addAttemp(gameUser: GameUser) {
    gameUser.attemps += 1;
    gameUser.udate = new Date();
    const result = await this.gameUserRepository.update(
      { game_user_id: gameUser.game_user_id },
      gameUser,
    );
    this.logger.log('result_update_add_attemp --> ' + result.affected);
  }
}
