import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity({
  name: 'os_wordle_game_user',
})
export class GameUser {
  @PrimaryGeneratedColumn()
  game_user_id: number;

  @Column({
    type: 'int',
    nullable: false,
  })
  game_id: number;
  @Column({
    type: 'int',
    nullable: false,
  })
  user_id: number;

  @Column({
    type: 'int',
    nullable: false,
    default: 0,
  })
  attemps: number;

  @Column({
    type: 'boolean',
    default: true,
    nullable: false,
  })
  enabled: boolean;
  @Column({
    type: 'timestamp',
    nullable: false,
    default: new Date(),
  })
  cdate: Date;
  @Column({
    type: 'timestamp',
    nullable: true,
  })
  udate: Date;
}
