import { Test, TestingModule } from '@nestjs/testing';
import { GameUserService } from './game.user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { GameUser } from './gameuser.entity';
import { GameModule } from '../game/game.module';
import { Word } from '../word/word.entity';
import { Game } from '../game/game.entity';
import { Wins } from '../wins/wins.entity';
import { User } from '../user/user.entity';

describe('GameUserService', () => {
  let service: GameUserService;
  const mockedRepo = {
    // mock the repo `findOneOrFail`
    update: jest.fn((criteria, update) => Promise.resolve({})),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [GameModule],
    })
      .overrideProvider(getRepositoryToken(User))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Wins))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Word))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(Game))
      .useValue(jest.fn())
      .overrideProvider(getRepositoryToken(GameUser))
      .useValue(mockedRepo)
      .compile();

    service = module.get<GameUserService>(GameUserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('add_attemp', async () => {
    const game = new GameUser();
    game.attemps = 0;
    game.game_user_id = 1;
    const res = await service.addAttemp(game);
    expect(res).toStrictEqual(undefined);
  });
});
