CREATE TABLE IF NOT EXISTS  os_wordle_user (
                    user_id serial PRIMARY KEY ,
                    username varchar(50) not null unique,
                    enabled bool not null  default true,
                    cdate TIMESTAMPTZ not null default now(),
                    udate timestamp
);
CREATE TABLE IF NOT EXISTS  os_wordle_game (
                    game_id serial PRIMARY KEY ,
                    enabled bool not null  default true,
                    active bool not null default false,
                    word  varchar(5)  not null,
                    expired_date int not null,
                    cdate timestamp not null  default  now(),
                    udate timestamp
);
 CREATE TABLE IF NOT EXISTS  os_wordle_game_user (
                    game_user_id serial PRIMARY KEY,
                    game_id int,
                    user_id int,
                    enabled bool not null  default true,
                    attemps int not null default 0,
                    cdate timestamp not null  default  now(),
                    udate timestamp,
                    CONSTRAINT fk_game
                       FOREIGN KEY(game_id)
                 	  REFERENCES os_wordle_game(game_id),
                    CONSTRAINT fk_user
                       FOREIGN KEY(user_id)
                 	  REFERENCES os_wordle_user(user_id)

                 );

CREATE TABLE IF NOT EXISTS  os_wordle_word (
                    word_id serial PRIMARY KEY ,
                    word VARCHAR(5) UNIQUE NOT NULL ,
                    enabled bool not null  default true,
                    is_use bool not null  default  false,
                    cdate timestamp not null  default  now(),
                    udate timestamp
                 );

CREATE TABLE IF NOT EXISTS  os_wordle_wins (
                    wins_id serial PRIMARY KEY ,
                    word varchar(5) not null ,
                    enabled bool not null  default true,
                    user_id int not null,
                    cdate timestamp not null  default  now(),
                    udate timestamp,
                    CONSTRAINT fk_user
                       FOREIGN KEY(user_id)
                 	  REFERENCES os_wordle_user(user_id),
                    unique(word, user_id)
                 );

insert into os_wordle_user(username, cdate) values ('emilio', now());
insert into os_wordle_user(username, cdate) values ('luis', now());
insert into os_wordle_user(username, cdate) values ('sofia', now());
insert into os_wordle_word(word ) values ('GATOS');
insert into os_wordle_word(word ) values ('ARBOL');
insert into os_wordle_word(word ) values ('PATAS');
