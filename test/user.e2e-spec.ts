import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';

import { INestApplication } from '@nestjs/common';
import { AppModule } from '../src/app.module';

describe('UserController', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = module.createNestApplication();

    await app.init();
  });

  it(`/GET user_stats`, async () => {

    const loginReq = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'luis' })
      .expect(200);
    const token = loginReq.body.access_token;
    return request(app.getHttpServer())
      .get('/user/stats/1')
      .set('Authorization', 'Bearer ' + token)
      .expect(200);
  });
  it(`/GET user_ranking`, async () => {

    const loginReq = await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'luis' })
      .expect(200);
    const token = loginReq.body.access_token;
    return request(app.getHttpServer())
      .get('/user/ranking')
      .set('Authorization', 'Bearer ' + token)
      .expect(200);

  });
  afterAll(async () => {
    await app.close();
  });
});
