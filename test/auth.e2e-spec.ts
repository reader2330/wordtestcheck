import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '../src/app.module';

describe('AuthController', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();

    await app.init();
  });
  it('/POST auth/login Authorized', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'luis' })
      .expect(200);
  });
  it('/POST auth/login Unauthorized', () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: 'l' })
      .expect({ msg: 'username_not_valid' })
      .expect(401);
  });
  afterAll(async () => {
    await app.close();
  });
});
